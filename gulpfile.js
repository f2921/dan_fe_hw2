"use strict";
const gulp = require("gulp"),
    clean = require("gulp-clean"),
    sass = require("gulp-sass")(require("sass")),
    browserSync = require("browser-sync").create(),
    concat = require("gulp-concat"),
    imagemin = require("gulp-imagemin"),
    autoprefixer = require("gulp-autoprefixer"),
    cleanCSS = require("gulp-clean-css"),
    jsMinify = require("gulp-js-minify");

const paths = {
    src: {
        img: "./src/img/*.png",
        styles: "./src/scss/**/*.scss",
        js: "./src/js/*.js",
    },
    dist: {
        self: "./dist",
        minCss: "./dist/css/",
        minJs: "./dist/js/",
        minImg: "./dist/img/",
    },
};
const cleanDist = () =>
    gulp.src(paths.dist.self, { allowEmpty: true }).pipe(clean());

const minScss = () =>
    gulp
        .src(paths.src.styles)
        .pipe(concat("style.min.css"))
        .pipe(sass().on("error", sass.logError))
        .pipe(
            autoprefixer({
                cascade: false,
            })
        )
        .pipe(cleanCSS({ compatibility: "ie8" }))
        .pipe(gulp.dest(paths.dist.minCss));

const minJs = () =>
    gulp
        .src(paths.src.js)
        .pipe(concat("scripts.min.js"))
        .pipe(jsMinify())
        .pipe(gulp.dest(paths.dist.minJs))
        .pipe(browserSync.stream());

const minImg = () =>
    gulp.src(paths.src.img).pipe(imagemin()).pipe(gulp.dest(paths.dist.minImg));

const watcher = () => {
    browserSync.init({
        server: {
            baseDir: "./",
        },
        notify: false,
        online: true,
    });
    gulp.watch(paths.src.styles, minScss).on("change", browserSync.reload);
    gulp.watch(paths.src.js, minJs).on("change", browserSync.reload);
};

gulp.task("build", gulp.series(cleanDist, minScss, minJs, minImg));
gulp.task("dev", watcher);
